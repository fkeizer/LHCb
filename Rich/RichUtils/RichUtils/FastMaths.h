
//-----------------------------------------------------------------------------
/** @file FastMaths.h
 *
 * Implementation of vaious fast mathematical functions for the Rich.
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 02/09/2007
 */
//-----------------------------------------------------------------------------

#pragma once

// LHCbMaths
#include "LHCbMath/FastMaths.h"

namespace Rich
{

  /** @namespace Rich::SIMD::Maths
   *
   *  Namespace for RICH SIMD Math functons
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   17/10/2017
   */
  namespace Maths
  {

    // Import from LHCbMath
    using namespace LHCb::Math;

  }
}
