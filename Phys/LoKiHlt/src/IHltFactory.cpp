// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/IHltFactory.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hybrid::IHltFactory
 *
 *  @date 2008-09-18
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
// ============================================================================
// The END
// ============================================================================
