<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE gdd SYSTEM "gdd.dtd">
<gdd>

<!-- **************************************************************************
  * XML-description of RichTraceMode class                                    *
  * author: Chris Jones   Christopher.Rob.Jones@cern.ch                       *
  * date:   2004-06-23                                                        *
  ************************************************************************* -->

  <package name='LHCbKernel'>
     <class
        name             = "RichTraceMode"
        author           = "Chris Jones   Christopher.Rob.Jones@cern.ch"
        desc             = "Helper class used to configure the ray tracing options in the IRichRayTracing tools"
        virtual          = "FALSE" >

        <enum name="RayTraceResult" desc="The result of the ray tracing" access="PUBLIC" >
          <enumval name="RayTraceFailed"  value="0"  desc="The ray tracing failed"/>
          <enumval name="OutsidePDPanel"  value="1"  desc="The ray tracing succeeded and but was outside the acceptance of the PD panels"/>
          <enumval name="InPDPanel"       value="2"  desc="The ray tracing succeeded and fell into the acceptance of an PD panel"/>
          <enumval name="InPDTube"        value="3"  desc="The ray tracing succeeded and fell into the acceptance of an PD tube"/>
        </enum>

        <method
          name    = "traceWasOK"
          desc    = "Tests if the given ray tracing result was successful, within the bounds presented by the configured mode"
          type    = "bool"
          virtual = "FALSE"
          const   = "TRUE">
          <arg
            type = "const LHCb::RichTraceMode::RayTraceResult"
            name = "result"/>
          <code>
             return ( (int)result >= (int)detPlaneBound() );
          </code>
        </method>

        <enum name="DetectorPrecision" desc="Flag the precision mode to use when calculating the PD acceptance on the PD panels. Only applies if DetectorPlaneBoundary enum is set to RespectPDTubes" access="PUBLIC" >
          <enumval name="FlatPDs"      value="0"  desc="Approximate the PD window acceptance by circles on a plane (fastest)"/>
          <enumval name="SphericalPDs" value="1"  desc="Models the PD entrance window by a simple sphere + circular acceptance (fast)"/>
          <enumval name="FullPDs"      value="2"  desc="Use the full detector description for the PDs (slower)"/>
        </enum>

        <enum name="DetectorPlaneBoundary" desc="Flag how to treat the PD panel and PD tube acceptances when performing the ray tracing" access="PUBLIC" >
          <enumval name="IgnorePDAcceptance" value="1"  desc="Ignore the PDs acceptance entirely (both the average PD panel and individual tubes)"/>
          <enumval name="RespectPDPanel"     value="2"  desc="Respect the average physical boundaries of the PDs on the panel plane"/>
          <enumval name="RespectPDTubes"     value="3"  desc="Respect the individual PD acceptances"/>
        </enum>

        <constructor
                 desc     = "Default Constructor"
                 initList = "m_data(0)" >
                <code>
                  setDetPlaneBound ( LHCb::RichTraceMode::IgnorePDAcceptance );
                  setDetPrecision  ( LHCb::RichTraceMode::FlatPDs            );
                </code>
        </constructor>

        <constructor
                desc     = "Constructor with configuration values"
                explicit = "TRUE"
                initList = "m_data(0)" >
                <arg const="TRUE" name="bound"                                     inout="BYVALUE" type="LHCb::RichTraceMode::DetectorPlaneBoundary" />
                <arg const="TRUE" name="detPrec = LHCb::RichTraceMode::FlatPDs"    inout="BYVALUE" type="LHCb::RichTraceMode::DetectorPrecision" />
                <arg const="TRUE" name="forcedSide = false"                        inout="BYVALUE" type="bool" />
                <arg const="TRUE" name="respectOuter = false"                      inout="BYVALUE" type="bool" />
                <arg const="TRUE" name="respectMirrSegs = false"                   inout="BYVALUE" type="bool" />
                <arg const="TRUE" name="checkBeamPipeIntersects = false"           inout="BYVALUE" type="bool" />
                <arg const="TRUE" name="checkPDKaptonShadowing = false"            inout="BYVALUE" type="bool" />
                <arg const="TRUE" name="aeroRefraction = false"                    inout="BYVALUE" type="bool" />
                <code>
                  setDetPlaneBound      ( bound           );
                  setDetPrecision       ( detPrec         );
                  setForcedSide         ( forcedSide      );
                  setOutMirrorBoundary  ( respectOuter    );
                  setMirrorSegBoundary  ( respectMirrSegs );
                  setBeamPipeIntersects ( checkBeamPipeIntersects );
                  setHpdKaptonShadowing ( checkPDKaptonShadowing );
                  setAeroRefraction     ( aeroRefraction          );
                </code>
        </constructor>

        <attribute
           desc = "Bit packed field containing the ray tracing configuration options"
           name = "data"
           init = "0"
           type = "bitfield" >
           <bitfield desc="The PD modelling precision to use"     length="4" name="detPrecision"   type="LHCb::RichTraceMode::DetectorPrecision" />
           <bitfield desc="The PD tube and panel acceptance mode" length="4" name="detPlaneBound"  type="LHCb::RichTraceMode::DetectorPlaneBoundary" />
           <bitfield desc="Flag to force the side to use in the ray-tracing"            length="1" name="forcedSide" />
           <bitfield desc="Flag to respect the overall outer boundaries of the mirrors" length="1" name="outMirrorBoundary" />
           <bitfield desc="Flag to respect the individual mirror segment boundaries (i.e. the small gaps between them)" length="1" name="mirrorSegBoundary" />
           <bitfield desc="Flag to turn on rejection of trajectories which intersect the beam pipe inside the RICH detectors" length="1" name="beamPipeIntersects" />
           <bitfield desc="Flag to turn on the checking for shadowing by the PD kapton" length="1" name="hpdKaptonShadowing" />
           <bitfield desc="Flag to turn on the refraction correction at the exit of the Aerogel volume into the Rich1Gas" length="1" name="aeroRefraction" />
        </attribute>

        <method
          name    = "fillStream"
          desc    = "Print this RichTraceMode in a human readable way"
          type    = "std::ostream&amp;"
          virtual = "FALSE"
          const   = "TRUE">
          <arg
            type = "std::ostream"
            name = "os"/>
        </method>

    </class>
  </package>
</gdd>
